﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkyController : MonoBehaviour {

	public Transform camera;

	public Transform skyDome;
	public GameObject cloudsPrefab;

	Vector3 domeOffset;
	Vector2 cloudPosition = new Vector2(-550, 10);
	Vector2 cloudDimension = new Vector2(550, 0);

	List<Transform> cloudObjects = new List<Transform>();

	void Awake () {
		domeOffset = skyDome.localPosition;
	}

	// Use this for initialization
	void Start () {
	
	}
	private int index = 0;
	// Update is called once per frame
	void Update () {
		skyDome.position = Vector3.Lerp(skyDome.position, new Vector3(camera.position.x, skyDome.position.y, 0), Time.deltaTime / 2f);

		if (GameController.instance.car.transform.position.x - cloudPosition.x >= 0) {
			if (cloudObjects.Count > 1) {
				cloudObjects[index].position = new Vector3 (cloudPosition.x + cloudDimension.x, cloudObjects[0].position.y, 0);
				index = (index + 1) % 2;
			} else {
				Transform t = (GameObject.Instantiate (cloudsPrefab) as GameObject).transform;
				t.position = new Vector3 (cloudPosition.x + cloudDimension.x, cloudPosition.y, 0);
				t.parent = transform;
				cloudObjects.Add (t);
			}
			cloudPosition.x += cloudDimension.x;
		}			
	}
}
