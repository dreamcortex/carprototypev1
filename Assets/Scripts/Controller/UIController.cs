﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	public static UIController instance;

	public Dropdown configEngine, configSlot;
	public InputField configName;
	public Slider configSpeed, configSuspension, configWeight, configBalance, configGravity, configFriction, configFlipAdjust;

	public Text speedText, suspensionText, weightText, balanceText, gravityText, frictionText, flipAdjustText, fastestTimeText, currentTimeText, healthText, bestText, carSpeedText;

	public Text cameraText, propsText, damageText, extraWheelText, rollerCoasterText, foregroundText;

	public Canvas worldCanvas;

	public GameObject coinScorePrefab, distanceIndicatorPrefab;

	public RectTransform hpBar, fuelBar;

	public Image carSpeedIndicator;

	void Awake () {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		if (rollerCoasterText != null)
			rollerCoasterText.transform.parent.GetComponent<Toggle>().isOn = (PlayerPrefs.GetInt (GameController.rollerCoasterPrefsKey) >= 0 ? false : true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	#region Public methods
	public void SetText (Text text, string s) {
		if (text != null)
			text.text = s;
	}

	public void SetInputField (InputField inputField, string s) {
		if (inputField != null)
			inputField.text = s;
	}

	public void SetRectTransformSize (RectTransform rectTransform, Vector2 v) {
		if (rectTransform != null)
			rectTransform.sizeDelta = v;
	}

	public void SetSelectableValue (Selectable selectable, float f ) {
		if (selectable != null) {
			if (selectable.GetType () == typeof(Dropdown)) {
				Dropdown dd = (Dropdown)selectable;
				dd.value = (int)f;
			} else if (selectable.GetType () == typeof(Slider)) {
				Slider s = (Slider)selectable;
				s.value = f;
			}
		}
	}

	public void SetSelectableOption (Selectable selectable, int i, string s) {
		if (selectable != null) {
			if (selectable.GetType () == typeof(Dropdown)) {
				Dropdown dd = (Dropdown)selectable;
				if (i < dd.options.Count) {
					dd.options [i].text = s;
				}
			}
		}
	}

	public float GetSelectableValue (Selectable selectable) {
		if (selectable != null) {
			if (selectable.GetType () == typeof(Dropdown)) {
				Dropdown dd = (Dropdown)selectable;
				return dd.value;
			} else if (selectable.GetType () == typeof(Slider)) {
				Slider s = (Slider)selectable;
				return s.value;
			}
		}
		return 0;
	}

	public void ChangeSpeed (bool b) {
		ChangeSlider (configSpeed, b);
		ConfigController.instance.ChangeSpeed (configSpeed);
	}

	public void ChangeSuspension (bool b) {
		ChangeSlider (configSuspension, b);
		ConfigController.instance.ChangeSuspension (configSuspension);
	}

	public void ChangeWeight (bool b) {
		ChangeSlider (configWeight, b);
		ConfigController.instance.ChangeWeight (configWeight);
	}

	public void ChangeBalance (bool b) {
		ChangeSlider (configBalance, b);
		ConfigController.instance.ChangeBalance (configBalance);
	}

	public void ChangeGravity (bool b) {
		ChangeSlider (configGravity, b);
		ConfigController.instance.ChangeGravity (configGravity);
	}

	public void ChangeFriction (bool b) {
		ChangeSlider (configFriction, b);
		ConfigController.instance.ChangeFriction (configFriction);
	}

	public void ChangeFlipAdjust (bool b) {
		ChangeSlider (configFlipAdjust, b);
		ConfigController.instance.ChangeFlipAdjust (configFlipAdjust);
	}
	#endregion

	private void ChangeSlider (Slider s, bool b) {
		if (s == null)
			return;
		s.value += (b ? 1 : -1) * (s.maxValue - s.minValue) / 10f;
	}
}
