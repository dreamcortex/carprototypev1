﻿using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour {

	public static LevelController instance;

	public Vector3 initialPosition;
	public Sprite background;
	public int carPrefabIndex;

	void Awake () {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		if (GameController.instance != null) {
			GameController.instance.car.transform.position = initialPosition + new Vector3 (0, GameController.instance.car.wheelRear.GetComponent<CircleCollider2D>().radius / 2f + GameController.instance.carBodyHeight, 0);
			GameController.instance.SwapCar (carPrefabIndex);
		}
		if (BackgroundController.instance != null) {
			BackgroundController.instance.InitializeBackground (background);
		}
	}
}
