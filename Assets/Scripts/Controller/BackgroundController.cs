﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackgroundController : MonoBehaviour {

	public static BackgroundController instance;

	public Sprite backgroundSprite;

	private Transform target;
	private Vector2 currentPosition;
	private Vector2 backgroundSize;
	private Vector3 backgroundOffset = new Vector3 (0, 50, 15);

	List<Transform> backgroundList = new List<Transform> ();

	void Awake () {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (target == null)
			return;

		for (int i = 0; i < backgroundList.Count; i++) {
			Vector3 pos = backgroundList [i].position;
			pos.y = Mathf.Clamp(target.position.y, 10, 70) / 2f - 14f + backgroundOffset.y;
			backgroundList [i].position = pos;
		}

		if (backgroundList.Count > 0) {
			// Move front to last
			if (target.position.x - currentPosition.x > backgroundSize.x / 2f) {
				backgroundList [0].position = backgroundList [2].position + new Vector3 (backgroundSize.x, backgroundSize.y, 0);
				backgroundList.Add (backgroundList [0]);
				backgroundList.RemoveAt (0);
				currentPosition.x += backgroundSize.x;
			}

			// Move last to front
			if (target.position.x - currentPosition.x < -backgroundSize.x / 2f) {
				backgroundList [2].position = backgroundList [0].position - new Vector3 (backgroundSize.x, backgroundSize.y, 0);
				backgroundList.Insert (0, backgroundList [2]);
				backgroundList.RemoveAt (3);
				currentPosition.x -= backgroundSize.x;
			}
		}
	}

	private GameObject CreateBackground (Vector3 currentPosition) {
		GameObject go = new GameObject ();
		go.transform.parent = transform;
		go.name = "Background";
		SpriteRenderer spriteRenderer = go.AddComponent<SpriteRenderer> ();
		spriteRenderer.sprite = backgroundSprite;
		go.transform.position = new Vector3 (currentPosition.x, currentPosition.y, 0) + backgroundOffset;
		return go;
	}

	public void InitializeBackground (Sprite sprite) {
		if (sprite != null) {
			backgroundSprite = sprite;
			Bounds bounds = backgroundSprite.bounds;
			backgroundSize.x = bounds.extents.x * 2f;

			currentPosition.x = -backgroundSize.x;
			for (int i = 0; i < 3; i++) {
				backgroundList.Add (CreateBackground (currentPosition).transform);
				currentPosition.x += backgroundSize.x;
			}
		}
		currentPosition = Vector2.zero;

		target = GameController.instance.car.transform;
	}
}
