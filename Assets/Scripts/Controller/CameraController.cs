﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public static CameraController instance;

	private Vector3[] positionOffset = {
		new Vector3(2, 10, -30),
		new Vector3(0, 0, -15),
		new Vector3(-5, 5, -15),
		new Vector3(20, 5, -15)
	};
	private Vector3[] targetOffset = {
		new Vector3(10, 0, 0),
		new Vector3(0, 0, 0),
		new Vector3(5, 0, 0),
		new Vector3(15, 0, 0)
	};

	private Transform followTarget;
	private int offsetIndex = 0;

	private Vector3 currentPositionOffset, currentTargetOffset;

	void Awake () {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		followTarget = GameController.instance.car.transform;
		offsetIndex = PlayerPrefs.GetInt ("Camera Offset");

		currentPositionOffset = positionOffset [0] * 0.5f;

		UIController.instance.SetText (UIController.instance.cameraText, "Camera " + (offsetIndex + 1));
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (followTarget == null)
			return;

		transform.position = followTarget.position + positionOffset[offsetIndex];
		transform.LookAt (followTarget.position + targetOffset[offsetIndex]);

		if (offsetIndex == 0) {
			float lerp = Mathf.Clamp01 (0.5f + GameController.instance.car.rigidbodyComponent.velocity.sqrMagnitude / 500f);
			transform.position = followTarget.position + Vector3.Lerp(currentPositionOffset, positionOffset [offsetIndex] * lerp, Time.deltaTime * 2f);
			currentPositionOffset = Vector3.Lerp(currentPositionOffset, positionOffset [offsetIndex] * lerp, Time.deltaTime * 2f);

			lerp = Mathf.Clamp01 (GameController.instance.car.rigidbodyComponent.velocity.sqrMagnitude / 500f);
			transform.LookAt(followTarget.position + Vector3.Lerp(currentTargetOffset, targetOffset[offsetIndex] * lerp + new Vector3(0, 5, 0), Time.deltaTime * 2f));
			currentTargetOffset = Vector3.Lerp(currentTargetOffset, targetOffset[offsetIndex] * lerp + new Vector3(0, 5, 0), Time.deltaTime * 2f);
		}
	}

	public void CycleCamera () {
		offsetIndex++;
		if (offsetIndex == positionOffset.Length)
			offsetIndex = 0;
		PlayerPrefs.SetInt ("Camera Offset", offsetIndex);
		UIController.instance.SetText (UIController.instance.cameraText, "Camera " + (offsetIndex + 1));
	}
}
