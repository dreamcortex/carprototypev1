﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventController : MonoBehaviour {

	public static EventController instance;

	private Transform car;

	public GameObject coinPrefab, bridgePrefab;
	public GameObject[] groundPrefabs;
	public GameObject triggerStart, triggerFinish, triggerReset;
	public GameObject boxPrefab, rocketPrefab, healthPrefab;
	public GameObject[] roadProps;

	private Vector2 coinCurrentPosition = new Vector2(50, 500);
	private Vector2 groundCurrentPosition = new Vector2 (0, 0);
	private Vector2 distanceIndicatorCurrentPosition = new Vector2 (0, 0);

	private float coinInterval = 2;

	[HideInInspector]
	public List<Transform> props = new List<Transform>();
	private List<Transform> grounds = new List<Transform> ();
	private Dictionary<Transform, List<Transform>> bridges = new Dictionary<Transform, List<Transform>> ();

	private float groundEndHeight = -4f;
	//private float groundOffset = -42.4f;
	private float groundOffset = -14.6f;
	//private float[] groundStartHeights = {42.4f, 42.86f, 51.85f, 44.18f, 35.29f, 42.82f};
	//private float[] groundEndHeights = {38.4f, 38.99f, 35.24f, 50.88f, 51.72f, 38.76f};
	//private float[] groundStartHeights = {15.75f};
	//private float[] groundEndHeights = {19.21f};
	private float[] groundStartHeights = {15.17f};
	private float[] groundEndHeights = {17.38f};
	private int groundIndex = 0;

	public PhysicsMaterial2D[] groundPhysics;

	private bool isEventEnabled = false;
	private string eventEnabledPrefsKey = "Event";

	private string foregroundPrefsKey = "Foreground";
	private bool foreground = false;

	void Awake () {
		instance = this;

		foreground = (PlayerPrefs.GetInt (foregroundPrefsKey) >= 0);
		isEventEnabled = (PlayerPrefs.GetInt (eventEnabledPrefsKey) >= 0);
	}

	// Use this for initialization
	void Start () {
		ToggleForeground (foreground);

		groundCurrentPosition.x = -groundPrefabs [0].transform.position.x * 2f;
		car = GameController.instance.car.transform;

		UIController.instance.SetText (UIController.instance.propsText, "Props " + (isEventEnabled ? "On" : "Off"));
	}

	// Update is called once per frame
	void Update () {
		if (car == null)
			return;

		if (isEventEnabled)
		if (coinCurrentPosition.x - car.position.x < 50f) {
			if (Random.value > 0.9f) {
				int number = Random.Range (3, 10);
				for (int i = 0; i < number; i++) {
					Vector2 origin = coinCurrentPosition;
					coinCurrentPosition.x += coinInterval;
					RaycastHit2D hit = Physics2D.Raycast (origin, -Vector2.up, 1000);
					if (hit != null) {
						Vector2 pos = hit.point;
						pos.y += 1f;
						GameObject go = GameObject.Instantiate (coinPrefab) as GameObject;
						go.transform.position = new Vector3 (pos.x, pos.y, 0);
						go.transform.parent = transform;
						props.Add (go.transform);
					}
				}
				coinCurrentPosition.x += Random.Range (10, 50);

				RaycastHit hit3D;
				int boxNumber = Random.Range (1, 6);
				for (int i = 0; i < boxNumber; i++)
					if (Physics.Raycast (new Vector3 (coinCurrentPosition.x - Random.Range(1, 5), coinCurrentPosition.y, 0), -Vector3.up, out hit3D, 1000)){
						GameObject go = GameObject.Instantiate (roadProps[Random.Range(0, roadProps.Length)]) as GameObject;
						go.transform.position = hit3D.point + new Vector3(0, i+1, Random.Range(1f, 1.5f) * (Random.value > 0.5? 1 : -1));
						go.transform.localEulerAngles = new Vector3 (0, 0, 0);
						go.transform.parent = transform;
						props.Add (go.transform);
					}

				if (Random.value > 0.0f)
				if (Physics.Raycast (new Vector3 (coinCurrentPosition.x - Random.Range(5, 10), coinCurrentPosition.y, 0), -Vector3.up, out hit3D, 1000)){
					GameObject go = GameObject.Instantiate (rocketPrefab) as GameObject;
					go.transform.position = hit3D.point + new Vector3(0, 1.5f, 0);
					go.transform.parent = transform;
					props.Add (go.transform);
				}

				if (Random.value > 0.5f)
				if (Physics.Raycast (new Vector3 (coinCurrentPosition.x - Random.Range(10, 15), coinCurrentPosition.y, 0), -Vector3.up, out hit3D, 1000)){
					GameObject go = GameObject.Instantiate (healthPrefab) as GameObject;
					go.transform.position = hit3D.point + new Vector3(0, 1.5f, 0);
					go.transform.parent = transform;
					props.Add (go.transform);
				}
			}
		}

		if (props.Count > 0)
			if (car.position.x - props [0].position.x > 500 || car.position.y - props[0].position.y > 100) {
				Transform t = props [0];
				props.RemoveAt (0);
				Destroy (t.gameObject);
			}

		if (grounds.Count > 4) {
			Transform t = grounds [0];
			grounds.RemoveAt (0);
			Destroy (t.gameObject);
		}


		if (groundCurrentPosition.x - car.position.x < 100) {
			GameObject go = GameObject.Instantiate (groundPrefabs[groundIndex]) as GameObject;
			Vector3 pos = new Vector3 (groundCurrentPosition.x, groundCurrentPosition.y, 0) + go.transform.position;
			groundCurrentPosition.x += go.transform.position.x * 2f;
			go.transform.position = pos;
			go.transform.parent = transform;
			//go.GetComponentInChildren<PolygonCollider2D> ().sharedMaterial = groundPhysics [Random.Range (0, groundPhysics.Length)];
			grounds.Add (go.transform);
			foreach (Transform t in  go.GetComponent<Road>().accelerator) {
				t.GetComponent<AreaEffector2D> ().forceMagnitude = GameController.instance.car.motorSpeed / 300f * GameController.instance.car.rigidbodyComponent.mass;
			}

			foreach (Transform t in go.GetComponentsInChildren<Transform>()) {
				if (t.name == "Foreground")
					t.gameObject.SetActive (foreground);
			}

			List<Transform> temp = new List<Transform> ();
			float bridgePrefabLength = bridgePrefab.GetComponent<BoxCollider2D> ().size.x;
			float nextGroundStartHeight = (groundIndex+1 == groundPrefabs.Length ? groundStartHeights [0] : groundStartHeights [groundIndex+1]);
			float heightDiff = Mathf.Abs (groundEndHeights [groundIndex] - nextGroundStartHeight);
			int bridgeNumber = (int)(heightDiff * 2f);
			bridgeNumber = 0;
			for (int i = 0; i < bridgeNumber; i++) {
				GameObject bridgeGO = GameObject.Instantiate(bridgePrefab) as GameObject;
				bridgeGO.transform.position = new Vector3(groundCurrentPosition.x + (i+0.5f) * bridgePrefabLength, groundCurrentPosition.y + groundEndHeights[groundIndex] + groundOffset - 0.5f, 0);
				bridgeGO.transform.parent = transform;
				//grounds.Add (bridgeGO.transform);
				temp.Add (bridgeGO.transform);
			}
			bridges.Add(go.transform, temp);

			if (groundIndex == 0) {
				GameObject start = GameObject.Instantiate (triggerStart) as GameObject;
				Vector3 localPos = start.transform.localPosition;
				Vector3 localScale = start.transform.localScale;
				start.transform.parent = go.transform;
				start.transform.localPosition = localPos;
				start.transform.localScale = localScale;

				GameObject reset = GameObject.Instantiate (triggerReset) as GameObject;
				localPos = reset.transform.localPosition;
				localScale = reset.transform.localScale;
				reset.transform.parent = go.transform;
				reset.transform.localPosition = localPos;
				reset.transform.localScale = localScale;
			}
			if (groundIndex == groundPrefabs.Length-1) {
				GameObject finish = GameObject.Instantiate (triggerFinish) as GameObject;
				Vector3 localPos = finish.transform.localPosition;
				Vector3 localScale = finish.transform.localScale;
				finish.transform.parent = go.transform;
				finish.transform.localPosition = localPos;
				finish.transform.localScale = localScale;
			}

			groundIndex++;
			if (groundIndex == groundPrefabs.Length)
				groundIndex = 0;

			for (int i = 0; i < temp.Count; i++) {
				pos = temp [i].position;
				temp [i].gameObject.AddComponent<Rigidbody2D> ();
				HingeJoint2D hinge = temp [i].gameObject.AddComponent<HingeJoint2D> ();
				hinge.enableCollision = true;
				if (i == 0)
					hinge.connectedAnchor = new Vector2 (temp [i].position.x, temp [i].position.y);
				if (i != 0) {
					hinge.connectedBody = temp [i - 1].gameObject.GetComponent<Rigidbody2D> ();
					hinge.connectedAnchor = new Vector2 ((temp [i].position.x - temp [i - 1].position.x) / temp [i].localScale.x, (temp [i].position.y - temp [i - 1].position.y));
				}
				if (i == temp.Count - 1) {
					hinge = temp [i].gameObject.AddComponent<HingeJoint2D> ();
					hinge.enableCollision = true;
					pos = temp [i].position;
					pos.y = groundStartHeights[groundIndex] + groundOffset - 0.25f;
					temp [i].position = pos;
					Vector2 lastHinge = new Vector2 (2, 0);
					pos.x += lastHinge.x;
					pos.y += lastHinge.y;
					hinge.anchor = lastHinge;
					hinge.connectedAnchor = pos;
				}
			}
			groundCurrentPosition.x += bridgeNumber * bridgePrefabLength;
		}

		if (UIController.instance.distanceIndicatorPrefab != null) {
			if (distanceIndicatorCurrentPosition.x - car.position.x < 100) {
				Transform distanceIndicator = (GameObject.Instantiate (UIController.instance.distanceIndicatorPrefab) as GameObject).transform;
				distanceIndicator.SetParent (UIController.instance.worldCanvas.transform);
				distanceIndicator.position = new Vector3(distanceIndicatorCurrentPosition.x, 0, 0) + new Vector3 (1.5f, 0f, -5.5f);
				distanceIndicator.GetComponent<UnityEngine.UI.Text> ().text = distanceIndicatorCurrentPosition.x + "m";
				distanceIndicatorCurrentPosition.x += 25;
				props.Add (distanceIndicator);
			}
		}
	}

	public void RecycleGround () {
		if (grounds.Count - 1 > groundPrefabs.Length) {
			for (int i = 0; i < groundPrefabs.Length; i++) {
				Transform t = grounds [0];
				foreach (Transform temp in bridges[t])
					Destroy (temp.gameObject);
				bridges.Remove (t);
				grounds.RemoveAt (0);
				Destroy (t.gameObject);
			}
		}
	}

	public void ToggleEvent () {
		isEventEnabled = !isEventEnabled;
		PlayerPrefs.SetInt (eventEnabledPrefsKey, (isEventEnabled ? 1 : -1));
		UIController.instance.SetText (UIController.instance.propsText, "Props " + (isEventEnabled ? "On" : "Off"));
	}

	public void ToggleForeground (bool b) {
		foreground = b;
		for (int i = 0; i < grounds.Count; i++) {
			grounds [i].GetComponent<Road> ().foreground.gameObject.SetActive (b);
		}
		UIController.instance.SetText (UIController.instance.foregroundText, "Foreground " + (b ? "On" : "Off"));
		PlayerPrefs.SetInt (foregroundPrefsKey, (b ? 1 : -1));
	}

	public void ToggleRollerCoaster (bool b) {
		GameController.instance.rollerCoaster = b;
		foreach (Road r in GetComponentsInChildren<Road>()) {
			r.rollerCoaster.gameObject.SetActive (GameController.instance.rollerCoaster);
		}

		UIController.instance.SetText (UIController.instance.rollerCoasterText, "360 " + (b ? "On" : "Off"));
		PlayerPrefs.SetInt (GameController.rollerCoasterPrefsKey, (GameController.instance.rollerCoaster ? -1 : 1));
	}
}
