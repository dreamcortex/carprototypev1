﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {

	public static AudioController instance;

	public AudioSource coinAudioSource, sharedAudioSource;
	public AudioClip coinSFX, hitSFX;

	private AudioSource engineAudioSource;

	void Awake () {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		engineAudioSource = GameController.instance.car.engineAudioSource;

		#if UNITY_EDITOR
		engineAudioSource.volume = 0;
		#endif
	}
	
	// Update is called once per frame
	void Update () {
		if (engineAudioSource != null)
			engineAudioSource.pitch = Mathf.Lerp(engineAudioSource.pitch, Mathf.Clamp (UserInputController.instance.inputX / 0.5f, 1f, 2f), Time.deltaTime);
	}

	public void PlayCoinSFX () {
		if (coinAudioSource.isPlaying)
			coinAudioSource.Stop ();
		coinAudioSource.PlayOneShot (coinSFX);
	}

	public void PlayHitSFX () {
		sharedAudioSource.PlayOneShot (hitSFX);
	}
}
