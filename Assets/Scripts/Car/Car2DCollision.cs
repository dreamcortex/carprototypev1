﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Car2DCollision : MonoBehaviour {

	private float flipTimer = 0;
	
	// Update is called once per frame
	void Update () {
		if (GameController.instance.car.bodyFlipped) {
			if (GameController.instance.car.rigidbodyComponent.velocity.magnitude < 1f && GameController.instance.car.rigidbodyComponent.angularVelocity < 1)
				flipTimer += Time.deltaTime;
			else
				flipTimer = 0;
		} else
			flipTimer = 0;

		if (flipTimer > 2)
			GameController.instance.car.ChangeHealth (-100);
	}

	void OnCollisionEnter2D (Collision2D coll) {
		if (coll.transform.tag == GameController.surfaceLava)
			GameController.instance.car.ChangeHealth (-100);
		if (coll.transform.tag == GameController.surfaceRoad) {
			GameController.instance.car.ChangeHealth (-coll.relativeVelocity.magnitude / 4f);
		}

		if (coll.transform.tag == GameController.surfaceRoad || coll.transform.tag == GameController.surfaceMovingPlatform)
			GameController.instance.car.bodyFlipped = true;
	}

	void OnCollisionStay2D (Collision2D coll) {
		//if (coll.transform.tag == GameController.surfaceRoad)
		//	GameController.instance.car.ChangeHealth (-coll.relativeVelocity.magnitude / 2f * Time.fixedDeltaTime / 10f);
	}

	void OnCollisionExit2D (Collision2D coll) {
		if (coll.transform.tag == GameController.surfaceRoad)
			GameController.instance.car.bodyFlipped = false;
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.name.Contains("Reset")) {
			GameController.instance.recordStarted = false;
			GameController.instance.currentGround = other.transform.parent.parent;
			/*GameController.instance.car.transform.position = new Vector3 (-265f + GameController.instance.currentGround.position.x, 2, 0);
			//GameController.instance.car.wheelFront.transform.localPosition = new Vector3 (1.25f, -0.5f, 0.1f);
			//GameController.instance.car.wheelRear.transform.localPosition = new Vector3 (-1.25f, -0.5f, 0.1f);
			GameController.instance.car.transform.rotation = Quaternion.identity;
			Rigidbody2D rb = GameController.instance.car._rigidbody;
			rb.velocity = Vector2.zero;
			rb.angularVelocity = 0;
			JointMotor2D motorFront = GameController.instance.car.motorFront;
			JointMotor2D motorRear = GameController.instance.car.motorRear;
			motorRear.motorSpeed = motorFront.motorSpeed = 0;
			GameController.instance.car.motorFront = motorFront;
			GameController.instance.car.motorRear = motorRear;*/
		}

		if (other.transform.parent.name.Contains("Start")) {
			if (GameController.instance.recordStarted)
				return;
			GameController.instance.recordStarted = true;
			if (EventController.instance != null)
				EventController.instance.RecycleGround ();
			GameController.instance.currentGround = other.transform.parent.parent;
			if (EventController.instance != null)
				EventController.instance.ToggleRollerCoaster (GameController.instance.rollerCoaster);
			GameController.instance.startTime = Time.time;
			GameController.instance.car.ChangeHealth (100);

			GameController.instance.currentRecord = new List<Vector3> ();
			GameController.instance.currentRecord.Clear ();
			GameController.instance.fastestRecordIndex = 0;
			GameController.instance.StartCoroutine ("Record");

			GameController.instance.car.ChangeFuel (int.MaxValue);
		}

		if (other.transform.parent.name.Contains("Finish")) {
			if (!GameController.instance.recordStarted)
				return;
			GameController.instance.recordStarted = false;

			GameController.instance.nextTargetRecord = Vector3.zero;
			GameController.instance.StopCoroutine ("Record");
			GameController.instance.currentRecord.Add (new Vector3 (GameController.instance.car.transform.position.x - GameController.instance.currentGround.position.x, GameController.instance.car.transform.position.y - GameController.instance.currentGround.position.y, GameController.instance.car.transform.eulerAngles.z));

			float time = Time.time - GameController.instance.startTime;
			GameController.instance.startTime = -1;
			if (time < PlayerPrefs.GetFloat ("Fastest")) {
				PlayerPrefs.SetFloat ("Fastest", time);
				UIController.instance.SetText (UIController.instance.fastestTimeText, GameController.FormatTime (time));
				GameController.instance.fastestRecord = GameController.instance.currentRecord;
				PlayerPrefsX.SetVector3Array ("Fastest Record", GameController.instance.currentRecord.ToArray ());
			}
		}

		/*if (other.name.Contains ("Roller")) {
			GameController.instance.car.EnterRollerCoaster (true, other.transform);
		}*/
	}

	/*void OnTriggerExit2D (Collider2D other) {
		if (other.name.Contains ("Roller")) {
			if (!other.GetComponent<RollerCoaster>().rightHalf.enabled)
				GameController.instance.car.EnterRollerCoaster (false, null);
		}
	}*/
}
