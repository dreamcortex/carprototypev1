﻿using UnityEngine;
using System.Collections;

public class Car3DCollision : MonoBehaviour {

	// Use this for initialization
	/*void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}*/

	void OnCollisionEnter (Collision coll) {
		Hit (coll, 2);
		AudioController.instance.PlayHitSFX ();
	}
	void OnCollisionStay (Collision coll) {
		Hit (coll);
	}

	void Hit (Collision coll, float forceMultiplier = 1) {
		if (coll.transform.name.Contains ("Barrier")) {
			//coll.transform.GetComponent<Rigidbody> ().AddForce (new Vector3 (2, 0, Random.Range(-0.5f, 0.5f)) * forceMultiplier, ForceMode.Impulse);
			coll.transform.GetComponent<Rigidbody>().AddForce(new Vector3(GameController.instance.car.rigidbodyComponent.velocity.x, 0, Random.Range(-1f, 1f)));
		}
	}

	void OnTriggerEnter (Collider other) {
		if (other.transform.parent.name.Contains ("Rocket")) {
			GameController.instance.car.PickupRocket ();
			if (EventController.instance != null)
				EventController.instance.props.Remove (other.transform);
			Destroy (other.gameObject);
		}

		if (other.tag == GameController.pickUpHealth) {
			GameController.instance.car.ChangeHealth (50f);
			if (EventController.instance != null)
				EventController.instance.props.Remove (other.transform);
			Destroy (other.gameObject);
		}

		if (other.tag == GameController.pickUpFuel) {
			GameController.instance.car.ChangeFuel (int.MaxValue);
			Destroy (other.gameObject);
		}
	}
}
