﻿using UnityEngine;
using System.Collections;

public class RollerCoaster : MonoBehaviour {

	public EdgeCollider2D leftHalf, rightHalf;
	public Transform center;

	// Use this for initialization
	void Start () {
		leftHalf.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D (Collision2D coll) {
		if (coll.transform.name.Contains ("Wheel Front") && !leftHalf.enabled && !GameController.instance.car.usingRollerCoaster) {
			GameController.instance.car.EnterRollerCoaster (true, center);
		}
	}

	void OnCollisionExit2D (Collision2D coll) {
		if (coll.transform.name.Contains ("Wheel Front") && !rightHalf.enabled && GameController.instance.car.usingRollerCoaster) {
			GameController.instance.car.EnterRollerCoaster (false, null);
			GameController.instance.car.rollerCoasterDownwardFlag = false;
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.transform.name.Contains ("Wheel Rear")) {
			if (!leftHalf.enabled)
				leftHalf.enabled = true;
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.transform.name.Contains ("Wheel Rear")) {
			if (rightHalf.enabled) {
				rightHalf.enabled = false;
				GameController.instance.car.rollerCoasterDownwardFlag = true;
			}
		}
	}
}
