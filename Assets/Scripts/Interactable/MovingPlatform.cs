﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

	private Rigidbody2D _rigidbody;
	private Vector3 _originalPos = Vector3.zero;
	public Vector3 offset = Vector3.zero;

	private bool _activated = false;

	private float _lerpDuration = 1f;
	private float _lerpTimer = 0;
	private int _lerpDirection = 1;

	void Awake () {
		_rigidbody = GetComponent<Rigidbody2D> ();
	}

	// Use this for initialization
	void Start () {
		_originalPos = transform.position;
		_lerpDuration = Mathf.Abs (Vector3.Magnitude (offset)) / 10f;
		_activated = false;
	}

	void FixedUpdate () {
		if (!GameController.instance.exploded && _activated) {
			_lerpTimer += _lerpDirection * Time.fixedDeltaTime;
			if (_lerpTimer > _lerpDuration || _lerpTimer < 0) {
				_lerpDirection = -_lerpDirection;
				_lerpTimer = (_lerpDirection > 0 ? 0 : _lerpDuration);
				if (_lerpDirection > 0)
					_activated = false;
			}
			float lerp = _lerpTimer / _lerpDuration;
			_rigidbody.MovePosition(Vector3.Lerp (_originalPos, _originalPos + offset, lerp));
		}
	}

	public void ActivatePlatform () {
		_activated = true;
	}
}
