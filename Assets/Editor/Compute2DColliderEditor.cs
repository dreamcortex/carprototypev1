﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Compute2DCollider))]
public class Compute2DColliderEditor : Editor {

	Compute2DCollider myTarget;

	private List<Vector2> _pointList = new List<Vector2> ();
	private Dictionary<int, string> _pointID = new Dictionary<int, string>();

	private void OnSceneGUI () {
		myTarget = (Compute2DCollider)target;

		if (myTarget.drawMode) {
			Event e = Event.current;
			Vector3 mousePosition = e.mousePosition;
			int controlID = GUIUtility.GetControlID (FocusType.Passive);
			EventType eventType = e.GetTypeForControl (controlID);
			if (eventType == EventType.mouseDown) {
				GUIUtility.hotControl = controlID;
				e.Use ();
			} else if (eventType == EventType.mouseDrag) {
				Ray ray = HandleUtility.GUIPointToWorldRay (new Vector2 (mousePosition.x, mousePosition.y));
				RaycastHit hit;
				if (Physics.SphereCast (ray, myTarget.radius * 3f, out hit)) {
					if (!_pointID.ContainsKey (hit.collider.GetInstanceID ())) {
						_pointID.Add (hit.collider.GetInstanceID (), "");
						hit.collider.GetComponent<MeshRenderer> ().material = myTarget.selectedPointMaterial;
						hit.collider.isTrigger = true;
						Vector3 point = hit.collider.transform.position;
						if (myTarget.relativePos)
							point -= myTarget.targetCollider.transform.position;
						_pointList.Add (new Vector2 (point.x, point.y));
					}
				}
				e.Use ();
			} else if (eventType == EventType.mouseUp) {
				GUIUtility.hotControl = 0;
				e.Use ();
			}
		}
		EditorUtility.SetDirty (target);
	}

	public override void OnInspectorGUI () {
		base.OnInspectorGUI ();

		myTarget = (Compute2DCollider)target;

		EditorGUILayout.BeginVertical ("box");
		EditorGUILayout.LabelField ("Catpure automatically");
		if (GUILayout.Button ("Capture side plane")) {
			AssignPointArray (OptimizePoint (CaptureSidePlane ()));
		}
		EditorGUILayout.EndVertical ();

		EditorGUILayout.BeginVertical ("box");
		EditorGUILayout.LabelField ("Draw manualy");
		if (GUILayout.Button ("Visualize Point")) {
			VisualizePoint ();
		}
		if (GUILayout.Button ("Clear Point")) {
			DestroyPoint ();
		}
		if (GUILayout.Button ("Assign Point")) {
			AssignPointArray (OptimizePoint (_pointList));
			DestroyPoint ();
		}
		EditorGUILayout.EndVertical ();
	}

	private void AssignPointArray (List<Vector2> list) {
		if (myTarget.targetCollider == null || list == null || list.Count == 0)
			return;

		if (myTarget.targetCollider.GetType () == typeof(PolygonCollider2D)) {
			PolygonCollider2D collider = (PolygonCollider2D)myTarget.targetCollider;
			collider.points = list.ToArray ();
		} else if (myTarget.targetCollider.GetType () == typeof(EdgeCollider2D)) {
			EdgeCollider2D collider = (EdgeCollider2D)myTarget.targetCollider;
			collider.points = list.ToArray ();
		}
	}

	private List<Vector2> OptimizePoint (List<Vector2> list) {
		List<Vector2> result = new List<Vector2> ();

		if (list.Count > 0) {
			Vector2 lastVertex = list [0];
			result.Add (list [0]);
			int iteration = list.Count;
			for (int i = 1; i < list.Count - 1; i++) {
				float distance = Vector2.Distance (lastVertex, list [i]);
				float slopeLeft = Mathf.Atan ((list [i].y - lastVertex.y) / (list [i].x - lastVertex.x)) * Mathf.Rad2Deg;
				float slopeRight = Mathf.Atan ((list [i + 1].y - list [i].y) / (list [i + 1].x - list [i].x)) * Mathf.Rad2Deg;
				if (Mathf.Abs (slopeRight - slopeLeft) > Mathf.Clamp (myTarget.slopeOptimization / distance, 0.1f, 1f)) {
					result.Add (list [i]);
					lastVertex = list [i];
				}
			}
			result.Add (list [list.Count - 1]);
		}
		return result;
	}

	private List<Vector2> CaptureSidePlane () {
		List<Vector2> result = new List<Vector2> ();

		Mesh mesh = myTarget.targetMesh.sharedMesh;
		Vector3[] vertices = mesh.vertices;

		List<Vector2> reverseList = new List<Vector2> ();

		/*result.Add (new Vector2 (vertices [3].x, vertices [3].y));
		result.Add (new Vector2 (vertices [0].x, vertices [0].y));
		reverseList.Add (new Vector2 (vertices [2].x, vertices [2].y));
		reverseList.Add (new Vector2 (vertices [1].x, vertices [1].y));

		for (int i = 4; i < vertices.Length; i++) {
			if (i % 2 == 0) {
				result.Add (new Vector2 (vertices [i].x, vertices [i].y));
			} else {
				reverseList.Add (new Vector2 (vertices [i].x, vertices [i].y));
			}
		}*/

		for (int i = 0; i < vertices.Length; i++) {
			if (vertices [i].y < myTarget.heightThreshold)
				reverseList.Add (new Vector2 (vertices [i].x, vertices [i].y));
			else
				result.Add (new Vector2 (vertices [i].x, vertices [i].y));
		}

		result.Sort ((a, b) => a.y.CompareTo (b.y));
		result.Sort ((a, b) => a.x.CompareTo (b.x));
		reverseList.Sort ((a, b) => a.x.CompareTo (b.x));

		for (int i = reverseList.Count - 1; i >= 0; i--) {
			result.Add (reverseList [i]);
		}

		if (myTarget.flipY)
			for (int i = 0; i < result.Count; i++)
				result[i] = Quaternion.Euler(0, 180, 0) * new Vector3(result[i].x, result[i].y, 0);

		if (!myTarget.relativePos) {
			for (int i = 0; i < result.Count; i++)
				result [i] += new Vector2 (myTarget.targetMesh.transform.position.x, myTarget.targetMesh.transform.position.y);
		}

		return result;
	}

	#region Draw Side plane
	private void VisualizePoint () {
		DestroyPoint ();

		Vector3[] vertices = myTarget.targetMesh.sharedMesh.vertices;
		for (int i = 0; i < vertices.Length; i++) {
			GameObject go = GameObject.CreatePrimitive (PrimitiveType.Cube);
			go.transform.parent = myTarget.targetMesh.transform;
			go.transform.localScale = Vector3.one * myTarget.radius;
			go.transform.localPosition = vertices [i];
		}
	}

	private void DestroyPoint () {
		_pointList.Clear ();
		_pointID.Clear ();

		Transform[] t = myTarget.targetMesh.transform.GetComponentsInChildren<Transform> ();
		for (int i = 0; i < t.Length; i++)
			if (t [i] != myTarget.targetMesh.transform)
				DestroyImmediate (t [i].gameObject);
	}
	#endregion
}
